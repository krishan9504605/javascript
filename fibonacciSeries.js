function fibonacci(n) {
    let fibSeries = [0, 1];

    for (let i = 2; i < n; i++) {
        fibSeries[i] = fibSeries[i - 1] + fibSeries[i - 2];
    }

    return fibSeries;
}

// Example: Generate the first 10 Fibonacci numbers
console.log(fibonacci(5)); // Output: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]

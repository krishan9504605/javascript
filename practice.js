// Online Javascript Editor for free
// Write, Edit and Run your Javascript code using JS Online Compiler

// const spinner = ['|','/','-','\\'];
// for (let i=0;i<spinner.length;i++){
//     setTimeout(() => {
//         console.clear();
//         console.log(spinner[i]);
//     }, 200); 
// }

const spinner = ['|', '/', '-', '\\'];
const delay = 200; // 200 milliseconds delay

function animateSpinner(spinner, delay) {
    for (let i = 0; i < spinner.length; i++) {
        setTimeout(() => {
            console.clear();
            console.log(spinner[i]);
        }, i * delay); // Multiply the delay by the index to stagger the prints
    }
}

// Repeat the spinner animation multiple times
function startSpinnerAnimation() {
    let iteration = 0;
    const repeatTimes = 10; // Number of times to repeat the spinner animation

    function repeat() {
        animateSpinner(spinner, delay);
        iteration++;
        if (iteration < repeatTimes) {
            setTimeout(repeat, spinner.length * delay);
        }
    }

    repeat();
}

startSpinnerAnimation();

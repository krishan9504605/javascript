//Swap the numbers using X-OR gate login, where 1 xor 1 and 0 xor 0 = 0 , others are always 1

let a = 23 //10111
let b = 99 // 1100011
function swapNum(num1,num2){
    num1 = num1 ^ num2 //num1 = 1110100
    num2 = num1 ^ num2 //num2 = 0010111 (23)
    num1 = num1 ^ num2 // num1 = 1100011 (99)
    console.log(num1) 
    console.log(num2)
}

swapNum(a,b)
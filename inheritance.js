class Animals{
    constructor(name,color,tail){
        this.name = name
        this.color = color
        this.tail = tail
    }

    run(){
        console.log(this.name + ' is running')
    }
    shouting(){
        console.log(this.name + ' is shouting' )
    }
}

class Monkey extends Animals{
    eatBanana(){
        console.log(this.name + ' is eating Banana')
    }
}

let pet_first = new Animals('Bruno','White','True')
let pet_second = new Monkey('Lucky')

pet_second.run()
pet_second.eatBanana()
pet_first.shouting()

//The concept to changes or update the method from super class into subclass is called Method overriding


class Employee{
    login(){
        console.log(`User successfull login`)
    }
    profile(){
        console.log(`User profile is completed`)
    }
    Salary(amount){
        console.log(`User Salary is ${amount} USD`)
    }

}

//Superclass is extends in Subclass here
class Developers extends Employee{
    leaves(number){
        console.log(`Developers has ${number} days leaves`)
    }
    // Salary(amount){
    //     console.log(`User Salary is ${amount+100} USD`)
    // }
    Salary(amount){
    super.Salary(6)
    console.log(`Salary is updated`)
    }

    Money(){
        console.log(this.Salary)
    }
}

let a = new Developers()
a.Salary(2000)
a.leaves(20)